import Vuex from 'vuex';
import Cookie from 'js-cookie'

const createStore = ()=>{
  return new Vuex.Store({
    state: {
      loadedPosts: [],
      token: null
    },
    mutations: {
      setToken(state, token){
        state.token = token;
      },
      clearToken(state){
        state.token = null;
      },
      setPosts(state, posts){
        state.loadedPosts = posts;
      },
      addPost(state, post){
        state.loadedPosts.push(post);
      },
      editPost(state, editedPost){
        const postIndex = state.loadedPosts.findIndex(post=>post.id === editedPost.id)
        state.loadedPosts[postIndex] = editedPost;
      }
    },
    actions: {
      authenticateUser(vuexContext, authData){
        var authUri = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='+process.env.fbAPIkey;
        if (!authData.isLogin){
          authUri = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key='+process.env.fbAPIkey;
        } 
        return this.$axios.$post(authUri, {
            email: authData.email,
            password: authData.password,
            returnSecureToken: true
          }).then(res=>{
            vuexContext.commit('setToken', res.idToken);
            localStorage.setItem('token', res.idToken);
            localStorage.setItem('tokenExpiration', new Date().getTime() + Number.parseInt(res.expiresIn * 1000));
            Cookie.set('jwt', res.idToken)
            Cookie.set('expirationDate', new Date().getTime() + Number.parseInt(res.expiresIn * 1000));
            // vuexContext.dispatch('setLogoutTimer', res.expiresIn*1000);
          }).catch(e=>{
            console.log("err: ", e);
          })
      },
      initAuth(vuexContext, req) {
        let token;
        let expirationDate
        if (req) { 
          if (!req.headers.cookie){
            return 
          }
          const jwtCookie = req.headers.cookie
            .split(';')
            .find(c => c.trim().startsWith("jwt="));
          if(!jwtCookie){
            return;
          }
          token = jwtCookie.split('=')[1];
          expirationDate = req.headers.cookie
            .split(';')
            .find(c => c.trim().startsWith("expiration="))
            .split("=")[1];
        } else {
          token = localStorage.getItem('token');
          expirationDate = localStorage.getItem('tokenExpiration');
        }
        if (new Date().getTime() > expirationDate || !token ) {
          console.log('Invalid or missing auth token')
          // vuexContext.commit('clearToken');
          vuexContext.dispatch('logout')
          return;
        }
        vuexContext.commit('setToken', token);
      },
      addPost(vuexContext, post){
        const createdPost = {
          ...post, 
          updatedDate: new Date() 
        }
        return this.$axios.$post('/posts.json?auth=' + vuexContext.state.token, createdPost)
        .then(res=>{
          vuexContext.commit('addPost', {
            ...createdPost,
            id: res.name
          });
          console.log('res: ', res);
        })
        .catch(err=>{
          console.error('err: ', err);
        })
      },
      editPost(vuexContext, editedPost){
        return this.$axios.$put("/posts/"+editedPost.id+".json?auth=" + vuexContext.state.token, editedPost)
        .then(res=>{
          vuexContext.commit('editPost', editedPost);
        })
        .catch(err=>console.log('error: ', err))
      },
      nuxtServerInit(vuexContext, context){
        return context.app.$axios.$get('/posts.json')
        .then(res=>{
          const postArray = [];
          for (const key in res) {
            postArray.push({ ...res[key], id: key })
          }
          vuexContext.commit('setPosts', postArray)
        })
        .catch(err=>{
          context.error(err);
        })
        // return new Promise((resolve, reject)=>{
        //   setTimeout(() => {
        //     vuexContext.commit('setPosts', [
        //       {
        //         id: "1",
        //         title: 'First Post',
        //         previewText: "Hey naw - my head's hurtin'",
        //         thumbnail: "https://blog.logrocket.com/wp-content/uploads/2019/04/1_gJkwvC01hwnQZd51ylvp9g.jpeg",
        //       }, 
        //       {
        //         id: "2",
        //         title: 'Another Post?',
        //         previewText: "Weren't we just here?!'",
        //         thumbnail: "https://blog.logrocket.com/wp-content/uploads/2019/04/1_gJkwvC01hwnQZd51ylvp9g.jpeg",
        //       },
        //     ])
        //     resolve();
        //     // resolve()
        //   }, 1500);
        //   // reject(new Error(err));
        // });
      },
      setPosts(vuexContext, posts){
        vuexContext.commit('setPosts', posts)
      },
      logout(vuexContext){
        vuexContext.commit('clearToken')
        Cookie.remove('jwt')
        Cookie.remove('expirationDate')
        if (process.client){
          localStorage.removeItem('token')
          localStorage.removeItem('tokenExpiration')
        }
      }
    },
    getters: {
      isAuthenticated(state){
        return state.token != null;
      },
      loadedPosts(state){
        return state.loadedPosts;
      }
    }
  });
}

export default createStore;