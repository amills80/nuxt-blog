export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'nuxt-blog',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'My nuxtJS blog webapp w/ Firebase services' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href:"https://fonts.googleapis.com/css2?family=M+PLUS+1:wght@200;400&family=M+PLUS+2:wght@400;600&display=swap"}
    ]
  },

  loading: {
    color: "#FAB617",
    height: "4px",
    // duration: 
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~assets/styles/main.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/global-components.js',
    '~plugins/date-filter.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],
  axios: {
    baseURL: process.env.BASE_URL || "https://nuxt-blog-bc40a-default-rtdb.firebaseio.com",
    credentials: false
  },
  generate: {
    // reserved for static site generation
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  env: {
    baseUrl: process.env.BASE_URL || "https://nuxt-blog-bc40a-default-rtdb.firebaseio.com",
    fbAPIkey: process.env.API_KEY || "AIzaSyD0fd0cyCY_rPe6edQby6tsJyesw3J1mZw"
  },
  transition: {
    name: 'fade',
    mode: 'out-in'
  }
}
